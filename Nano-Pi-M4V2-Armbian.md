# M4NAS with a NanoPi M4v2 running Armbian
<!--toc-->
- [M4NAS with a NanoPi M4v2 running Armbian](#m4nas-with-a-nanopi-m4v2-running-armbian)
    * [Install Armbian 64bits](#install-armbian-64bits)
    * [Enable Hardware acceleration [Out-dated]](#enable-hardware-acceleration-out-dated)
    * [OpenMediaVault (Optional)](#openmediavault-optional)
        * [Installation](#installation)
        * [Additional useful plugins:](#additional-useful-plugins)
    * [PWM fan:](#pwm-fan)
    * [OTG mass storage](#otg-mass-storage)
    * [Change boot device](#change-boot-device)
    * [Time sync](#time-sync)
    * [Pull data from old server with Rsync](#pull-data-from-old-server-with-rsync)
    * [Network configuration with systemd](#network-configuration-with-systemd)
    * [Partition and format disks](#partition-and-format-disks)
    * [Mount drives](#mount-drives)
        * [MergerFS](#mergerfs)
    * [SnapRAID](#snapraid)
        * [Compile SnapRAID](#compile-snapraid)
        * [Configuration with 3 disks: 2 data and 1 parity](#configuration-with-3-disks-2-data-and-1-parity)
        * [Periodic sync and scrub](#periodic-sync-and-scrub)
    * [Hard Disk Spin down](#hard-disk-spin-down)
        * [NOT WORKING ! Percistance with hdparm.conf](#not-working--percistance-with-hdparmconf)
    * [SSH authentification by Key only](#ssh-authentification-by-key-only)
    * [System backup using dd (@TODO)](#system-backup-using-dd-todo)
    * [NFS share](#nfs-share)
        * [Server side](#server-side)
        * [Client side with Autofs](#client-side-with-autofs)
    * [Samba share (@TODO more config details)](#samba-share-todo-more-config-details)
        * [Web Service Discovery (WSD)](#web-service-discovery-wsd)
    * [Anti-virus with ClamAV (@TODO: Update !)](#anti-virus-with-clamav-todo-update-)
        * [Virus event](#virus-event)
        * [Give up on Daemon and run scan with cron weekly](#give-up-on-daemon-and-run-scan-with-cron-weekly)
- [Version control system](#version-control-system)
    * [Git](#git)
    * [Subversion (svn)](#subversion-svn)
- [Users and groups](#users-and-groups)
    * [VPN](#vpn)
        * [OpenVPN](#openvpn)
    * [Docker](#docker)
    * [Encrypted backups using Duplicity and Duply](#encrypted-backups-using-duplicity-and-duply)
        * [Backup another server to the NAS](#backup-another-server-to-the-nas)
        * [Offsite encrypted backup tothe cloud (pCloud) using rclone](#offsite-encrypted-backup-tothe-cloud-pcloud-using-rclone)
    * [GPG encryption an compression](#gpg-encryption-an-compression)

<!-- tocstop -->

## Install Armbian 64bits

- Download link: [NanoPI M4v2 - Armbian](https://www.armbian.com/nanopi-m4-v2/#kernels-archive-all)
    
- unpack the archive
    
- Check the binary file
    
    `shasum -c Armbian_20.02.1_Nanopim4v2_buster_legacy_4.4.213.img.sha`
    
- Flash Armbian to an SD card:
    
    `sudo dd if=Armbian_20.02.1_Nanopim4v2_buster_legacy_4.4.213.img of=/dev/sdX bs=4M status=progress conv=fsync`
    
- Log into the NanoPi with ssh after finding out which IP address your router gave it:
    
    `ssh root@192.168.x.x` the default password is "1234"
    
- Follow the instructions (choose new root password, create sudo user, etc)
    
- Flash Armbian to the eMMC module:
    
    `nand-sata-install` then choose option 1 "Boot from eMMC"
    
- Reboot
    
- Login again and update all packages:
    
    `sudo apt update -y && sudo apt upgrade -y`
    

## Enable Hardware acceleration \[Out-dated\]
References:

- [RK3399 Legacy Multimedia Framework | Armbian Forum](https://forum.armbian.com/topic/16516-rk3399-legacy-multimedia-framework/)

Instructions OUTDATED

- Download the package on the forum page: [\[Development\] RK3399 media script](https://forum.armbian.com/topic/9272-development-rk3399-media-script/)
    
- Copy the package from you computer to the NanoPi using scp:
    
    `scp media-rk3399_1.0.txz <user>@<ip>:~`
    
- Login to the NanoPi with ssh
    
- uncompress the archive:
    
    `tar xvf media-rk3399_*.txz`
    
- enter the uncompressed folder and run the installation script:

```
cd media-script
./media-rk3399.sh
```

## OpenMediaVault (Optional)
### Installation
Run the installation script from OpenMediaVault repository and be patient:

`wget -O - https://github.com/OpenMediaVault-Plugin-Developers/installScript/raw/master/install | sudo bash`

Alternatively, OMV can be installed from *armbian-config* under the section *Software* \> *Softy*.

### Additional useful plugins:

```bash
sudo apt install -y \
openmediavault-backup \
openmediavault-clamav \
openmediavault-diskstats \
openmediavault-snapraid \
openmediavault-symlinks \
openmediavault-unionfilesystems
```

## PWM fan:

References:

- [PWM Fan on Nanopi M4 ??](https://forum.armbian.com/topic/11086-pwm-fan-on-nanopi-m4/)
- [NanoPI M4](https://forum.armbian.com/topic/7511-nanopi-m4/page/10/?tab=comments#comment-92709)

Installation:

- Get the fan control script and copy it to the right location:
    
    `sudo cp start-rk3399-pwm-fan.sh /usr/bin`
    
- Get the service definition file and copy it to the right location:
    
    `sudo cp pwm-fan.service /etc/systemd/system`
    
- Enable the new service to run it automaticaly at system start-up:
    
    `sudo systemctl enable pwm-fan.service`
    
- Optionally, start the service right now to give it a try:
    
    `sudo systemctl start pwm-fan.service`
    

## OTG mass storage
If we want to use our server also as a DAS (Device Attached storage),
we can utilize the OTG from the USB type C port, to allow mounting our drives like external mass storage device another computer.

- Add *g\_mass\_storage* to */etc/modules* then reboot:

```
echo "g_mass_storage" | sudo tee -a /etc/modules
sudo reboot -h now
```

- To mount 1 drive using OTG in removable mode, call *modprobe* like so (replace *X* and *Y* to match your device):
    
    `sudo modprobe g_mass_storage file=/dev/sdXY stall=0 removable=y`
    
- To unload the module:
    
    `sudo rmmod g_mass_storage`
    
- To mount 2 drives:
    
    `sudo modprobe g_mass_storage file=/dev/sda1,/dev/sdb1 removable=y,y`
    
- To auto-mount at system boot:
    
```
crontab -e
@reboot * * * * * modprobe g_mass_storage file=/dev/sda1,/dev/sdb1 removable=y,y
```

## Change boot device

Edit the UUID in */boot/armbianEnv.txt* to select another boot device. E.g: to boot from SD card again.
To find the UUID of the device use `sudo fdisk -l` to list storage devices and then `sudo blkid | grep UUID` to list devices UUIDs.

## Time sync

References:

- [Digital Ocean - How to set up time synchronization on debian 10](https://www.digitalocean.com/community/tutorials/how-to-set-up-time-synchronization-on-debian-10)
- [Ask Ubuntu - Enable system clock synchronization](https://askubuntu.com/questions/1046214/enable-system-clock-synchronization)

Set the HW clock to system time:

```bash
sudo hwclock -w
```

Sychronize system clock:

```bash
sudo nano /etc/systemd/timesyncd.conf
```

```
[Time]
NTP=0.europe.pool.ntp.org
FallbackNTP=0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org
```

```bash
sudo systemctl daemon-reload  
sudo timedatectl set-ntp false  
sudo timedatectl set-ntp true
```

## Pull data from old server with Rsync

Data sync:

```bash
rsync -av --progress \
--exclude='data/Download' \
--exclude='data/Public/Game/install' \
--exclude='data/Backup' \
/srv/dev-disk-by-id-ata-ST6000DM003-2CY186_WCT211CR-part1/data nanopi@192.168.0.4:/mnt/volume1/
```

Container sync:

```bash
rsync -av --progress \
--exclude=letsencrypt \
--exclude=mayan-edms \
--exclude=nginx-proxy \  
--exclude=pihole \
--exclude=plex \
--exclude=traefik \
--exclude=wireguard \
/srv/dev-disk-by-id-ata-ST6000DM003-2CY186_WCT211CR-part1/containers/ nanopi@192.168.0.4]:~/containers/
```

## Network configuration with systemd

References:

- [Arch Linux Wiki - Systemd-network](https://wiki.archlinux.org/index.php/Systemd-networkd)

Network configuration files are located in `/etc/systemd/network/`.
Create network configuration files for a wired, static address, interface:

```bash
sudo nano /etc/systemd/network/20-wired.network
```

Insert the following:

```
[Match]  
Name=eth0

[Network]  
Address=192.168.0.4/24  
Gateway=192.168.0.1  
DNS=192.168.0.3  
#DNS=8.8.8.8
```

Here, *eth0* is the interface name, we can find it using `sudo ifconfig`.
*192.168.0.4* is the static address we want for our machine.
*192.168.0.3* is the address of our DNS server, here PiHole runnning on a Raspberry Pi.

After restart, the machine with be accessible with the new IP address we just define and PiHole will be use as DNS.

## Partition and format disks
Use `fdisk` for partitioning, using **gpt** partition table.
Use `mkfs.ext4 /dev/sdX -b 4096 -L diskY` to format the partition using EXT4 file system with a block size of 4069 bits and give a label.

## Mount drives

First, we create amounting point for our drive in `/mnt/disk1`.
We can choose any directory we like as mounting point.
We traditionally use `/mnt` or `/media` for mounting location.

```bash
sudo mkdir /mnt/disk1
```

Change the permission to allow read and execute to all, this is important for network shares later:

```bash
sudo chmod 755 /mnt/disk1
```

We now want to modify the file `/etc/fstab` and add mounting instructions for our drive there to have it automatically monted at startup, but first we need to fine the identity of our drive.
We can find the UUID of all storage devices with `sudo blkid` and deduce which one we need by using, for example `sudo df -h`.

Note: the *UUID* will change if the drive is re-formatted, but unlike */dev/sdxy*, it survives reboot.

We can also use *LABEL* instead of *UUID* if the drive was formatted with one.

In my case, I add this line to */etc/fstab*:

```bash
LABEL=disk1 /mnt/disk1 ext4 defaults 0 2
```

### MergerFS
References:

- [MergerFS(1) | Debian Manpages](https://manpages.debian.org/testing/mergerfs/mergerfs.1.en.html)

MergerFS allow us to make multiple drives appear as one big drive.
Files existing in different physical drives will appear together in a merged directory structure.

First, we mount a new drive just like we did before:

```bash
sudo mkdir /mnt/disk2  
sudo chmod 755 /mnt/disk2
```

Now we create one more mounting point that is going to be our merged volume:

```bash
sudo mkdir /mnt/volume1  
sudo chmod 755 /mnt/volume1
```

Assuming the drive is formated with a matching partition label, we had the following to `/etc/fstab`:

```bash
LABEL=disk2 /mnt/disk2 ext4 defaults 0 2
```

Now we install MergerFS:

```bash
sudo apt install mergerfs
```

We just have to add a final line to `/etc/fstab` to mount our merged filesystem:

```
/mnt/disk*	/mnt/volume1	fuse.mergerfs	defaults,allow_other,hard_remove,use_ino,minfreespace=100G	0	0
```

- defaults: set all default options
- allow_other: all other users than the one mounting the merged file system
- hard_remove: temporary files are not used and files are deleted immediately
- use_ino: Causes mergerfs to supply file/directory inodes rather than libfuse (important for Syncthing)
- minfreespace: threshold to write to another drive.

## SnapRAID
References:

- [Manual | SnapRAID.it](https://www.snapraid.it/manual)
- [Combining Different Sized Drives with mergerfs and SnapRAID | SelfHostedHome.com](https://selfhostedhome.com/combining-different-sized-drives-with-mergerfs-and-snapraid/)
- [ Chronial / snapraid-runner | GitHub](https://github.com/Chronial/snapraid-runner)
- [ larssont / snapraid-runner | GitHub](https://github.com/larssont/snapraid-runner)

### Compile SnapRAID
SnapRAID is not part of Armbian nor Debian repository.
We need to download the source code and compile it on the NAS.
Let start by creating a `src` directory and work from there:

```bash
mkdir ~/src
cd ~/src
```

Now we download SnapRAID source package (check the website for the latest version):

```bash
wget https://github.com/amadvance/snapraid/releases/download/v11.5/snapraid-11.5.tar.gz
```

To unpack, compile and test, follow the instructions from GitHub: [INSTALL](https://github.com/amadvance/snapraid/blob/master/INSTALL)
The **make** and **make check** steps take a lot of time, so go prepare a cup of tea and spend some time away from the computer.
If everythin goes well SnapRAID should be installed under `/usr/local/bin/snapraid`.

### Configuration with 3 disks: 2 data and 1 parity
Previously, we have formated and mounted 3 disks:

- disk1: first data disk, mounted at /mnt/disk1
- disk2: second data disk, mounted at /mnt/disk2
- diskp: parity disk, mounted at /mnt/parity

It's time to configure SnapRAID to do its thing with those disks.
I have decided to make SnapRAID run as **nanopi** instead of root,
therefore we have to carefully set permissions all along the setup process.

We create and edit the file `/etc/snapraid.conf` and write the following content:

```
# Defines the file to use as parity storage
# It must NOT be in a data disk
# Format: "parity FILE_PATH"
parity	/mnt/parity/snapraid.parity

# Defines the files to use as content list
# You can use multiple specification to store more copies
# You must have least one copy for each parity file plus one. Some more don't
# hurt
# They can be in the disks used for data, parity or boot,
# but each file must be in a different disk
# Format: "content FILE_PATH"
content /var/snapraid/snapraid.content
content /mnt/disk1/.snapraid.content
content /mnt/disk2/.snapraid.content

# Defines the data disks to use
# The order is relevant for parity, do not change it
# Format: "disk DISK_NAME DISK_MOUNT_POINT"
data d1 /mnt/disk1/
data d2 /mnt/disk2/

# Excludes hidden files and directories (uncomment to enable).
#nohidden
 
# Defines files and directories to exclude
# Remember that all the paths are relative at the mount points
# Format: "exclude FILE"
# Format: "exclude DIR/"
# Format: "exclude /PATH/FILE"
# Format: "exclude /PATH/DIR/"
exclude *.unrecoverable
exclude /tmp/
exclude /lost+found/
exclude *.!sync
exclude .AppleDouble
exclude ._AppleDouble
exclude .DS_Store
exclude ._.DS_Store
exclude .Thumbs.db
exclude .fseventsd
exclude .Spotlight-V100
exclude .TemporaryItems
exclude .Trashes
exclude .AppleDB
exclude .stfolfer
exclude .stversion
```

We must give this file read access to **nanopi**:

```bash
sudo chmod 644 /etc/snapraid.conf
```

SnapRAID needs to access and write to the following directories:
- /mnt/parity/
- /var/snapraid/
- /mnt/disk1/
- /mnt/disk2/
Make sure the permissions are set correctely for the **nanopi** user:

```bash
nanopi@m4nas:~$ ll -a /mnt/
total 24
drwxr-xr-x  6 root   root  4096 Dec 18 14:05 .
drwxr-xr-x 19 root   root  4096 Dec 18 13:56 ..
drwxr-xr-x  7 nanopi users 4096 Dec 19 07:01 disk1
drwxr-xr-x  4 nanopi users 4096 Dec 19 07:01 disk2
drwxr-xr-x  3 nanopi users 4096 Dec 18 14:28 parity
drwxr-xr-x  7 nanopi users 4096 Dec 19 07:01 volume1
```

The directory `/var/snapraid` doesn't exist yet, lets create it and give it the right ownership and permissions:

```bash
sudo mkdir /var/snapraid
sudo chown nanopi:nanopi /var/snapraid
sudo chmod 755 /var/snapraid
```

Later on we will also need a log location, lets create it and set it up now:

```bash
sudo mkdir /var/log/snapraid
sudo chown nanopi:nanopi /var/log/snapraid
sudo chmod 755 /var/log/snapraid
```

It is time to make our first **sync**.This step is very long (for 3TB it took me near 24 hours).
During this process, no local data must change.
Lets stop all our services that might write to our data disks:

```bash
sudo service nfs-kernel-server stop
sudo service smbd stop
sudo docker stop
```

Now we run the **sync** command and we can let it do it's thing for a while:

```bash
snapraid sync
```

### Periodic sync and scrub
We are going to use a nice python script that performs all the necessary SnapRAID tasks and notifies us with a report.
The script from the original creator, [Chronial](https://github.com/Chronial/snapraid-runner), only supports e-mail notification,
but there is an unmerged pull-request from [larssont](https://github.com/larssont/snapraid-runner) that implements Telegram notifications. We will use that.
First we go to our `src/` directory and we clone larssont's repo:

```bash
git clone https://github.com/larssont/snapraid-runner.git
```

We copy the configuration exemple to `/etc/` and we edit it with our preferences:

```bash
sudo cp ~/src/snapraid-runner/snapraid-runner.conf.example /etc/snapraid-runner.conf
```

```
[snapraid]
; path to the snapraid executable (e.g. /bin/snapraid)
executable = /usr/local/bin/snapraid
; path to the snapraid config to be used
config = /etc/snapraid.conf
; abort operation if there are more deletes than this, set to -1 to disable
deletethreshold = 40
; if you want touch to be ran each time
touch = false

[logging]
; logfile to write to, leave empty to disable
file = /var/log/snapraid/snapraid.log
; maximum logfile size in KiB, leave empty for infinite
maxsize = 5000

[notifications]
; if you want notifications to be activated
on = true
; set to false to get full program output via notifications
short = true

[email]
; when to send an email, comma-separated list of [success, error]
;sendon = success,error
;subject = [SnapRAID] Status Report:
;from =
;to =
; maximum email size in KiB
;maxsize = 500

[smtp]
host =
; leave empty for default port
port =
; set to "true" to activate
ssl = false
tls = false
user =
password =

[telegram]
; when to send a message, comma-separated list of [success, error]
sendon = success,error
; Telegram bot token
token = 1414994961:AAG06HN7PDXyPmsc-Ei0VBJ6Kzpm9mwnvew 
; chat_id for telegram bot
chat_id = 99488064

[scrub]
; set to true to run scrub after sync
enabled = true
percentage = 12
older-than = 10
```

We must give this file read access to **nanopi**:

```bash
sudo chmod 644 /etc/snapraid-runner.conf
```

All that is left to do is to create a cron job to run the **scrub** and **sync** periodically via **snapraid-runner**:

```bash
crontab -e
```

```bash
# SnapRAID sync and scrub every day at 0:00
0 0 * * *       python3 /opt/snapraid-runner/snapraid-runner.py --conf /etc/snapraid-runner.conf
```

## Hard Disk Spin down

We can set drives to stand-by mode after a define time of inactivity using *hdparm*.
From the man page we can find how to set the time, as it is not a linear parameter:

> A value of zero means "timeouts are disabled": the device will not
> automatically enter standby mode. Values from 1 to 240 specify multiples of
> 5 seconds, yielding timeouts from 5 seconds to 20 minutes. Values from 241
> to 251 specify from 1 to 11 units of 30 minutes, yielding timeouts from 30
> minutes to 5.5 hours. A value of 252 signifies a timeout of 21 minutes. A
> value of 253 sets a vendor-defined timeout period between 8 and 12 hours,
> and the value 254 is reserved. 255 is interpreted as 21 minutes plus 15
> seconds. Note that some older drives may have very different interpreta‐
> tions of these values.

Lets set out drive 1 with a spin down timeout of 30 minutes:

```bash
sudo hdparm -S 241 /dev/sdb
```

However, *hdparm* commands are not surviving reboot.
Also `/dev/sdb` is subject to change, it is preferable to use *Label* or *UUID*.

Next we create a *systemd* service to set our *hdparm* configuration at system start-up en resume from suspend.
create a file `/etc/systemd/system/hddsilence.service` or copy it from `m4nas/sw/hddsilence.service`:

```
[Unit]
Description=Silence Hard Disks
After=suspend.target

[Service]
Type=oneshot
# Disable automatic head parking for the Toshiba Disk:
ExecStart=/sbin/hdparm -B 254 /dev/disk/by-label/disk1
# Enable automatic spin down after 30 minutes:
ExecStart=/sbin/hdparm -S 241 /dev/disk/by-label/disk1
ExecStart=/sbin/hdparm -S 241 /dev/disk/by-label/disk2
ExecStart=/sbin/hdparm -S 241 /dev/disk/by-label/diskp

[Install]
WantedBy=suspend.target basic.target
```

Now we activate the service:

```bash
sudo systemctl enable hddsilence.service
```

And we reload the daemons:

```bash
sudo systemctl daemon-reload
```

### NOT WORKING ! Percistance with hdparm.conf

Edit `/etc/hdparm.conf` like so:

```
## Spindown 30min disk1, reduce noise at the cost of performance
/dev/disk/by-label/disk1 {
    spindown_time = 241
    acoustic_management = 128
    write_cache = on
}

## Spindown 30min disk2, reduce noise at the cost of performance
/dev/disk/by-label/disk2 {
        spindown_time = 241
        acoustic_management = 128
        write_cache = on
}
```

## SSH authentification by Key only

References:

- [How To Configure SSH Key-Based Authentication on a Linux Server](https://www.digitalocean.com/community/tutorials/how-to-configure-ssh-key-based-authentication-on-a-linux-server)

Assuming we already have a SSH key pair on our computer, we just send out public key too the NAS using *ssh-copy-id* like so:

```bash
ssh-copy-id nanopi@192.168.0.4
```

We enter the same password we use to connect to ssh before.

Alternatively, we can provide the public key manually to the remote by copying the content of our local *~/.ssh/id_rsa.pub* into the remote *~/.ssh/authorized_keys*.

Next we disable password authentication on the remote.
For this we just change `PasswordAuthentication` to `no` in `/etc/ssh/sshd_config`.

## System backup using dd (@TODO)

References:

- [GitHub - Openmediavault-Plugin](https://github.com/OpenMediaVault-Plugin-Developers/openmediavault-backup/blob/master/usr/sbin/omv-backup)

I made a script and use a weekly cron task in root crontab.

## NFS share

References:

- [Ubuntu Wiki Fr - NFS](https://doc.ubuntu-fr.org/nfs)
- [Armbian Forum - HOWTO : NFS Server and Client Configuration](https://forum.armbian.com/topic/1482-howto-nfs-server-and-client-configuration/)

### Server side

Install necessary packages:

```bash
sudo apt install nfs-kernel-server
```

Configure the shares by editing `/etc/exports`:

```
/mnt/volume1/data/Backup 192.168.0.0/24(fsid=1,rw,sync,subtree_check)
/mnt/volume1/data/Document 192.168.0.0/24(fsid=2,rw,sync,subtree_check)
/home/nanopi/Download 192.168.0.0/24(fsid=3,rw,sync,subtree_check)
/mnt/volume1/data/Music 192.168.0.0/24(fsid=4,rw,sync,subtree_check)
/mnt/volume1/data/Other 192.168.0.0/24(fsid=5,rw,sync,subtree_check)
/mnt/volume1/data/Picture 192.168.0.0/24(fsid=6,rw,sync,subtree_check)
/mnt/volume1/data/Public 192.168.0.0/24(fsid=7,rw,sync,subtree_check)
/mnt/volume1/data/Video 192.168.0.0/24(fsid=8,rw,sync,subtree_check)
```

### Client side with Autofs

Install `autofs`:

```bash
sudo apt install autofs
```

Create a mounting point for our NFS share folders:

```bash
sudo mkdir /media/m4nas
```

Create a file `/etc/auto.nfs` with the following content:

```
Backup	-fstype=nfs,rw  m4nas:/mnt/volume1/data/Backup
Document	-fstype=nfs,rw  m4nas:/mnt/volume1/data/Document
Download	-fstype=nfs,rw  m4nas:/mnt/volume1/data/Download
Music	-fstype=nfs,rw	m4nas:/mnt/volume1/data/Music
Other	-fstype=nfs,rw  m4nas:/mnt/volume1/data/Other
Picture	-fstype=nfs,rw  m4nas:/mnt/volume1/data/Picture
Public	-fstype=nfs,rw  m4nas:/mnt/volume1/data/Public
Video	-fstype=nfs,rw  m4nas:/mnt/volume1/data/Video
```

Now, we point to this configuration file by editing `/etc/auto.master` and adding this:

```
/media/m4nas /etc/auto.nfs --ghost,--timeout=120
```

Finally, we restart `autofs` service:

```bash
sudo systemctl restart autofs.service
```

## Samba share (@TODO more config details)

Install required packages:

```bash
sudo apt install samba
```

Create a user that will have access to the shared folders:

```bash
sudo smbpasswd -a antoine
```

Note: this is not a UNIX user !

Define shared folders editing `/etc/samba/smb.conf`, for example:

```
[Backup]
comment = Backup archives on M4NAS
path = /mnt/volume1/data/Backup/
guest ok = no
guest only = no
read only = no
browseable = yes
inherit acls = yes
inherit permissions = no
ea support = no
store dos attributes = no
printable = no
create mask = 0664
force create mode = 0664
directory mask = 0775
force directory mode = 0775
hide special files = yes
follow symlinks = yes
hide dot files = yes

[Public]
comment = Public files on M4NAS
path = /mnt/volume1/data/Public/
guest ok = yes
guest only = no
read only = no
browseable = yes
inherit acls = yes
inherit permissions = no
ea support = no
store dos attributes = no
printable = no
create mask = 0664
force create mode = 0664
directory mask = 0775
force directory mode = 0775
hide special files = yes
follow symlinks = yes
hide dot files = yes
```

Activate the NetBIOS name server to allow discovery the clients file explorer:

```bash
sudo systemctl --now enable nmbd.service
```

### Web Service Discovery (WSD)

References:

- [GitHub christgau/wsdd](https://github.com/christgau/wsdd)

In order for our network share to show-up automatically in the the file explorer of our clients machines,
we ned to install `wsdd`, the Web Service Discovery Daemon, from a third party repository.

We start by adding the repository to `/etc/apt/sources.list.d/`:

```bash
sudo touch /etc/apt/sources.list.d/wsdd.list && echo "deb https://pkg.ltec.ch/public/ $(lsb_release -cs) main" | tee -a /etc/apt/sources.list.d/wsdd.list
```

Add the repository public key to the trusted list:

```bash
curl -fsSL https://pkg.ltec.ch/public/conf/ltec-ag.gpg.key | sudo apt-key add -
```

We update the package list and install `wsdd`:

```bash
sudo apt update && sudo apt install wsdd
```

Before staring the service, we have some configuration to do.
First we need to edit the service in `/lib/systemd/system/wsdd.service` to bind with `smbd`:

```
[Unit]
Description=Web Services Dynamic Discovery host daemon
After=network-online.target
Wants=network-online.target
BindsTo=smbd.service

[Service]
Type=simple
EnvironmentFile=/etc/wsdd.conf
ExecStart=/usr/bin/wsdd $WSDD_PARAMS
ExecReload=/bin/kill -HUP $MAINPID
Restart=on-failure
User=nobody
Group=nogroup

[Install]
WantedBy=multi-user.target
```

And we also modify the configuration `/etc/wsdd.conf` like so:

```
# command line parameters for wsdd (consult man page)
WSDD_PARAMS="--shortlog --workgroup='WORKGROUP'"
```

Now, set some firewall rules to allow multicast on port 3702:
- incoming and outgoing traffic to udp/3702 with multicast source/destination:
  - 239.255.255.250 for IPv4
  - ff02::c for IPv6
- outgoing unicast traffic from udp/3702
- incoming to tcp/5357
You should further restrict the traffic to the (link-)local subnet, e.g. by using the fe80::/10 address space for IPv6.

```bash
sudo ufw allow proto udp from 239.255.255.250 to any port 3702
sudo ufw allow proto udp from ff02::c  to any port 3702
sudo ufw allow out proto udp from 192.168.1.0/24 to any port 3702
sudo ufw allow in from 192.168.1.0/24 to any port 5357
```

Finally, we activate the service and reload daemons:

```bash
sudo systemctl enable wsdd.service && sudo systemctl daemon-reload
```

## Anti-virus with ClamAV (@TODO: Update !)

References:

- [ClamAV - Documentation - On-Access scanning](https://www.clamav.net/documents/on-access-scanning#configuration-and-recipes)
- [Arch Wiki - ClamAV](https://wiki.archlinux.org/index.php/ClamAV#OnAccessScan)

Install required packages:

```bash
sudo apt install clamav clamav-daemon
```

Add the user *clamav* to the group *users* to allow clamav-daemon to scan files.

```bash
sudo usermod -a -G users clamav
```

Edit `/etc/systemd/system/clamav-daemon.service.d/extend.conf` and add *-p* to the *mkdir* command like so:

```
[Service]
ExecStartPre=-/bin/mkdir -p /run/clamav
ExecStartPre=/bin/chown clamav /run/clamav
```

### Virus event

Log virus event:

```bash
#!/bin/bash
PATH=/usr/bin
alert="Signature detected: $CLAM_VIRUSEVENT_VIRUSNAME in $CLAM_VIRUSEVENT_FILENAME"
echo "$(date) - $alert" >> /var/log/clamav/detections.log
```

### Give up on Daemon and run scan with cron weekly

```bash
sudo crontab -e
```

```
0 2 * * 2 freshclam && clamscan --log=/var/log/clamav/detections.log --move=/mnt/disk1/quarantine -r /home/ /mnt/

```
# Version control system

## Git

Install Git:

```bash
sudo apt install git
```

## Subversion (svn)

Install Subversion:

```bash
sudo apt install subversion
```

Create a Subversion user and group:

```bash
sudo groupadd svn
```

```bash
sudo useradd -g svn -G users,ssh svn
```

# Users and groups

Create a unix user named *antoine* and with the main group set to *users*:

```bash
sudo useradd -g users antoine
```

Set a password for this new user:

```bash
sudo passwd antoine
```

## VPN

### OpenVPN

Install `openvpn` directely on the NAS:

```
sudo apt install openvpn
```

Go to the server and generate the configuration file thanks to [Nyr's script](https://github.com/Nyr/openvpn-install).  
Then, copy the content of the generated *.ovpn* and past it in a new file `/etc/openvpn/client/vps-raynard-space.conf` on the client machine, here the NAS.

Enable `openvpn` service on the client machine:

```
sudo systemctl --now enable openvpn
sudo systemctl --now enable openvpn-client@vps-raynard-space
```

## Docker

References:

- [Managing access tokens | Docker Documentation](https://docs.docker.com/docker-hub/access-tokens/)
- [Docker login Fails with error message “Error saving credentials:..."| Docker Forum](https://forums.docker.com/t/docker-login-fails-with-error-message-error-saving-credentials-cannot-autolaunch-d-bus-without-x11-display/77789)

Multiple docker engines exists and can be installed, e.g: docker.io, docker-ce (Community Edition) or moby-engine (for Fedora).

Here we use **docker.io**, and **docker-compose** to create and manage stacks, we also need some extra package to login to docker-hub:

```bash
sudo apt install docker.io docker-compose gnupg2 pass
```

## Encrypted backups using Duplicity and Duply
References:

- [Duplicity man page | nongnu.org](https://www.nongnu.org/duplicity/vers8/duplicity.1.html)
- [GilGalaad / duplicity-rclone | Github.com](https://github.com/GilGalaad/duplicity-rclone)
- [Duplicity | Debian Wiki](https://wiki.debian.org/Duplicity)
- [Creating Encrypted FTP Backups With duplicity And duply On Debian Squeeze | HowToForge](https://www.howtoforge.com/creating-encrypted-ftp-backups-with-duplicity-and-duply-on-debian-squeeze)

Duplicity provides encrypted and recursive backup trough many backends, such as ssh, rsync and ftp.
The backups are encrypted and signed using GPG.

Duply makes it simple to setup and configure automatic backups.

Here, make a very basic setup, backing up the entire system, excluding the data drives.
We also use the simplest way to encrypt the files, using a password that will be used to encrypt and decrypt the backups (it's also the least secure, but I consider it good enough for the usecase).

Install the required software:

```bash
sudo apt install duplicity duply
```

Now, because we are look for backing uo system files under */* directory, we need to do the setup as root:
Login as root using `su` and type root's password.

Go to root's directory using `cd`.

We start by creating a duply profile, named **m4nas** (you can use whatever name you wish):

```bash
duply m4nas create
```

Now, we create a configuration:

```bash
nano /root/.duply/m4nas/conf
```

And we add the content from the exemple file and customize it (here I removed most of the stuff):

```
# gpg encryption settings, simple settings:
#  GPG_KEY='disabled' - disables encryption alltogether
#  GPG_KEY='<key1>[,<key2>]'; GPG_PW='pass' - encrypt with keys,
#   sign if secret key of key1 is available use GPG_PW for sign & decrypt
#  Note: you can specify keys via all methods described in gpg manpage,
#        section "How to specify a user ID", escape commas (,) via backslash (\)
#        e.g. 'Mueller, Horst', 'Bernd' -> 'Mueller\, Horst, Bernd'
#        as they are used to separate the entries
#  GPG_PW='passphrase' - symmetric encryption using passphrase only
#GPG_KEY='Key ID'
GPG_PW='superstrongpassword'

# backend, credentials & location of the backup target (URL-Format)
# generic syntax is
#   scheme://[user[:password]@]host[:port]/[/]path
# eg.
#   sftp://bob:secret@backupserver.com//home/bob/dupbkp
# for details and available backends see duplicity manpage, section URL Format
#   http://duplicity.nongnu.org/duplicity.1.html#sect7
# BE AWARE:
#   some backends (cloudfiles, S3 etc.) need additional env vars to be set to
#   work properly, read after the TARGET definition for more details.
# ATTENTION:
#   characters other than A-Za-z0-9.-_.~ in the URL have to be
#   replaced by their url encoded pendants, see
#     http://en.wikipedia.org/wiki/Url_encoding
#   if you define the credentials as TARGET_USER, TARGET_PASS below duply
#   will try to url_encode them for you if the need arises.
TARGET='file:///mnt/volume1/data/Backup/m4nas'

# base directory to backup
SOURCE='/'
# Time frame for old backups to keep, Used for the "purge" command.  
# see duplicity man page, chapter TIME_FORMATS)
MAX_AGE=6M

# Additinal Duplycity parameters, here the location of the logfile
DUPL_PARAMS="$DUPL_PARAMS --log-file /var/log/backup/duplicity.log"
```

We have to exclude a bunch of directories from the backup, because we backup to the same computer,
and also because a bunch of "files" in Linux are actually **no files**.
We create the following file:

```bash
nano /root/.duply/exampleuser/exclude
```

And add the following content:

```
# although called exclude, this file is actually a globbing file list
# duplicity accepts some globbing patterns, even including ones here
# here is an example, this incl. only 'dir/bar' except it's subfolder 'foo'
# - dir/bar/foo
# + dir/bar
# - **
# for more details see duplicity manpage, section File Selection
# http://duplicity.nongnu.org/duplicity.1.html#sect9
/dev
/proc
/sys
/run
/mnt
/tmp
/media
/home/nanopi/Download
```

It's important to have the backup target location in this list, for me it is under */mnt*.

Finally, because we do a local backup (*file:///*), we want to correct ownership of the backup files after the backup is complete.
To do this, we provide duply with a *post* script to execute:

```bash
nano /root/.duply/exampleuser/post
```

```bash
chmod -R nanopi:nanopi /mnt/volume1/data/Backup/m4nas
```

Only step left, it schedule a periodic backup.
I just create a cron task to execute the backup every week.
Duply will take care of the rest:

```bash
crontab -e
```

```bash
# Backup system using duplicity and duply
0 3 * * Tue	/usr/bin/duply m4nas backup
```

### Backup another server to the NAS
I also want to backup my VPS onto my NAS in the same way.
For this I just need a slightly diffrent conf file for Duply, to use rsync (or ssh).
Also, my VPS and NAS are connected together through a VPN server.

```
# gpg encryption settings, simple settings:
#  GPG_KEY='disabled' - disables encryption alltogether
#  GPG_KEY='<key1>[,<key2>]'; GPG_PW='pass' - encrypt with keys,
#   sign if secret key of key1 is available use GPG_PW for sign & decrypt
#  Note: you can specify keys via all methods described in gpg manpage,
#        section "How to specify a user ID", escape commas (,) via backslash (\)
#        e.g. 'Mueller, Horst', 'Bernd' -> 'Mueller\, Horst, Bernd'
#        as they are used to separate the entries
#  GPG_PW='passphrase' - symmetric encryption using passphrase only
#GPG_KEY='_KEY_ID_'
GPG_PW='unbreakablepassword'

# base directory to backup
SOURCE='/'

# backend, credentials & location of the backup target (URL-Format)
# generic syntax is
#   scheme://[user[:password]@]host[:port]/[/]path
# eg.
#   sftp://bob:secret@backupserver.com//home/bob/dupbkp
# for details and available backends see duplicity manpage, section URL Format
#   http://duplicity.nongnu.org/duplicity.1.html#sect7
# BE AWARE:
#   some backends (cloudfiles, S3 etc.) need additional env vars to be set to
#   work properly, read after the TARGET definition for more details.
# ATTENTION:
#   characters other than A-Za-z0-9.-_.~ in the URL have to be
#   replaced by their url encoded pendants, see
#     http://en.wikipedia.org/wiki/Url_encoding
#   if you define the credentials as TARGET_USER, TARGET_PASS below duply
#   will try to url_encode them for you if the need arises.
TARGET='rsync://nanopi@10.8.0.9//mnt/volume1/data/Backup/vps'

# Time frame for old backups to keep, Used for the "purge" command.  
# see duplicity man page, chapter TIME_FORMATS)
MAX_AGE=6M

DUPL_PARAMS="$DUPL_PARAMS --log-file /var/log/backup/duplicity.log"
```

No *post* script needed here.
However, we need to authorized the server to connect to the NAS using key authentication.

We have to generate a *ssh* key pair for the root user on the VPS, if not already done:

```
su
cd
ssh-keygen
```

Now we copy the public key of the VPS into the file `/home/nanopi/.ssh/authorized_keys` on the NAS.
Read the public key like so:

```bash
cat /root/.ssh/id_rsa.pub
```

### Offsite encrypted backup tothe cloud (pCloud) using rclone

References:

- [pCloud | RCLONE](https://rclone.org/pcloud/)
- [Invalid ‘access_token’ with pCloud | RCLONE Forum](https://forum.rclone.org/t/invalid-access-token-with-pcloud/19710/9)

The stable repository of Debian (Buster) has a version of **rsync** that is old and not fully compatible with pCloud.
We need to activate and configure the **testing** repository in order to install a newer version.
It is generally not recommended to mix stable, testing or unstable repositories, it is very easy to break Debian this way.
However, we don't take to much risk with **rclone**, because it doesn't have much dependencies.
We can verify this with `apt depends rclone`.

First off, we edit `/etc/apt/source.list` and add the testing repository by appending the following line:

```
# Testing:
deb http://deb.debian.org/debian testing main
```

Now we configure APT to never use the testing repository by default by giving it a low priority.
For this we create preference file `/etc/apt/preference.d/global` with this content:

```
Package: *
Pin: release a=stable
Pin-Priority: 500

Package: *
Pin: release a=testing
Pin-Priority: 50
```

Then we create a second preference file that update only **rclone** using testing `/etc/apt/preference.d/rclone`:

```
Package: rclone
Pin: release a=testing
Pin-Priority: 600
```

Now **testing** has a higher update priority than **stable**, for **rclone** only.

We update the package cache and install rclone:

```bash
sudo apt update && sudo apt install rclone
```

Finally we can configure rclone to work with pCloud, for this we need to install rClone also on a machine with a desktop-environment and a web-browser...
On the NAS, run the next command and follow the instruction. It is important to also for though **advance configuration** and change the **hostname** to **eapi.pcloud.com**:
```
rclone config
```

When is comes to run a command on the DE machine, use the follwing flag:
```
rclone authorize "pcloud" --pcloud-hostname=eapi.pcloud.com
```

Once the configuration is complete on the NAS, check the correct hostname is set in `.config/rclone/rclone.conf`.
If everyting looks good we can try the connection like this:
```
rclone ls remote:
```
This should list all the files stored in pCloud.

Now we can sync our backup directory periodically using a cron job:

```bash
crontab -e
```

```bash
# rclone Backup to pCloud every Wednesday at 1:00
0 1 * * 3       rclone sync --log-file=~/rclone.log --log-level INFO /mnt/volume1/data/Backup remote:/Backup > ~/rclone.log
```

## GPG encryption an compression
- [How to create compressed encrypted archives with tar and gpg | LinuxConfig.org](https://linuxconfig.org/how-to-create-compressed-encrypted-archives-with-tar-and-gpg)
- [Fixing GPG "Inappropriate ioctl for device" errors](https://d.sb/2016/11/gpg-inappropriate-ioctl-for-device-errors)
- [https://superuser.com/questions/162624/how-to-password-protect-gzip-files-on-the-command-line | Superuser.com](https://superuser.com/questions/162624/how-to-password-protect-gzip-files-on-the-command-line)

